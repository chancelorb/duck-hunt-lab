// This is a jQuery "ready" closure:
jQuery(function() {
  // - Unlike a normal closure, we don't have to call it
  // - jQuery will call it when the page is loaded

  // I'll do some of the work for you, first I will grab the <body></body>
  var body = $('body');

  // 1. Can you create a <div> with the class "duck" and name it "duck"
  //const duck = $('<div>').addClass('duck');
  //body.append(duck);
  // 2. Next, use setInterval to toggle the "flap" class on the duck every 250 ms (1/4 second)
  //const flapFunc = function() {
    //duck.toggleClass('flap')
  //}
  //flapFunc();
  //setInterval(flapFunc, 250);
  // 3. Fantastic!  Now, let's move the duck using CSS "top" and "left"

  //const top = $('.duck').css('bottom', 100)
  //const left = $('.duck').css('right', 100)
  /*function multiDuck () {
    for (i = 0; i < 5; i++) {
      const duck = $('<div>').addClass('duck');
      body.append(duck);
      setInterval(function() {duck.toggleClass('flap')});
      setInterval(function() {
      duck.css('top', Math.random() * window.innerHeight),
      duck.css('left', Math.random() * window.innerWidth)
    }, 2000);
    }
    return duck;
  }*/

  //multiDuck()

let duck = $(".duck");
let count;

  function createDuck() {
    count = 5;
    for (let i = 0; i < 5; i++) {
      let newDuck = $('<div>').addClass('duck');
      body.append(newDuck);
      setInterval(function() {newDuck.toggleClass('flap'); }, 250);
      setInterval(function() {newDuck.css('top', Math.random() * window.innerHeight), newDuck.css('left', Math.random() * window.innerWidth)} , 1000);
    }
    duck = $(".duck")
    return duck
  };
  createDuck();

  function destroyDuck(e) {
    $(e).addClass('shot');
    setInterval(function() {$('.shot').remove();}, 1000);
    count--;
    console.log('clicked');
  };

  duck.click(function(e) {
    console.log(e)
    let event = e.target
    destroyDuck(event)
  });


  function checkForWinner() {
    let r = confirm('You win! Play again?');
    if (r === true) {
      window.location.reload();
    }

  };

  setInterval(function(){
    if (count === 0 || count < 0) {
      checkForWinner();
    };
    console.log(count)
  }, 3000);



  //function checkForWinner() {
  //  if ($('div') === 0)
  //};
  //checkForWinner()

  // 4. Try making the duck move to a different location after 1 second
  //setInterval(function() {duck.toggleClass('right')}, 1000)
  // 5. Congratulations! Move on to part 2!

  // ---------------------------- PART 2 ---------------------------------

  // 6. Things are getting a bit messy. Let's create
  //    a "function" called createDuck() that does everything in 1-4
  //    and "returns" the duck object

  // 7. Now, let's create lots of ducks!  Use a "for" loop to create 5 ducks
  //    using our fancy new createDuck() function

  // 8a. Uh oh, our ducks are overlapping.  Modify createDuck so each time
  //    it creates a duck, it appears in a random location
  // 8b. The ducks should also move to a random location after 1 second
  // HINT: Use Math.random() * window.innerWidth    for "left"
  //       And Math.random() * window.innerHeight   for "top"

  // 9. Our ducks are going to be easy targets if they only move once.
  //    Modify createDuck() so the ducks keep moving around

  // 10. Keep going! Move onto part 3!

  // --------------------------- PART 3 ------------------------------------

  // 11. BOOM. Attach a "click" handler that adds the "shot" class to
  //     the duck when you click on it!

  // 12. After a duck has been clicked on, remove it from the DOM after
  //     a short delay (1 second)

  // 13. Create a new function named checkForWinner() that reads the DOM
  //     to see if there are any ducks left. If not, alert "YOU WIN!"

  // 14. BONUS: The ducks are moving pretty erratically, can you think
  //     of a way to adjust the ducks speed based on how far needs to move?

  // 15. BONUS: Add the "left" and "right" class to the duck based on the
  //     direction the duck is flying

  // FIN. You win 1 trillion tokens.  Play the day away!
})
